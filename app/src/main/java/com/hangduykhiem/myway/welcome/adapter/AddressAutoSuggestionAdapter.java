package com.hangduykhiem.myway.welcome.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.hangduykhiem.myway.R;
import com.hangduykhiem.myway.data.model.AddressModel;
import com.hangduykhiem.myway.data.model.AddressModel.AddressFeatures;
import com.hangduykhiem.myway.welcome.WelcomePresenter;
import com.hangduykhiem.myway.welcome.adapter.AddressAutoSuggestionAdapter.AutosuggestionViewHolder;
import java.util.ArrayList;
import java.util.List;

/**
 * An adapter that help recyclerView display Address AutoSuggestion
 *
 * Created by dean on 21.4.2017.
 */

public class AddressAutoSuggestionAdapter extends RecyclerView.Adapter<AutosuggestionViewHolder> {

  List<AddressFeatures> addressFeaturesList;
  WelcomePresenter welcomePresenter;

  public AddressAutoSuggestionAdapter(WelcomePresenter welcomePresenter) {
    addressFeaturesList = new ArrayList<>();
    this.welcomePresenter = welcomePresenter;
  }

  @Override
  public AutosuggestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    Context context = parent.getContext();
    LayoutInflater inflater = LayoutInflater.from(context);

    // Inflate the custom layout
    View contactView = inflater.inflate(R.layout.row_address_suggestion, parent, false);

    // Return a new holder instance
    return new AutosuggestionViewHolder(contactView, welcomePresenter);
  }

  @Override
  public void onBindViewHolder(AutosuggestionViewHolder holder, int position) {

    // Get real Address from model
    holder.bindData(addressFeaturesList.get(position));
  }

  public void setNewSuggestion(AddressModel addressModel) {
    if (addressModel == null) {
      addressFeaturesList = new ArrayList<>();
    } else {
      addressFeaturesList = addressModel.features;
    }
    notifyDataSetChanged();
  }

  @Override
  public int getItemCount() {
    return addressFeaturesList.size();
  }

  static class AutosuggestionViewHolder extends RecyclerView.ViewHolder {

    TextView nameTextView;
    AddressFeatures currentAddress;

    AutosuggestionViewHolder(View itemView, final WelcomePresenter welcomePresenter) {
      super(itemView);

      nameTextView = (TextView) itemView.findViewById(R.id.row_address_suggestion_text);
      itemView.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          welcomePresenter.onAddressSuggestionClick(currentAddress);
        }
      });
    }

    void bindData(AddressFeatures addressFeatures) {
      String address =
          addressFeatures.properties.name + ", " + addressFeatures.properties.postalcode;
      nameTextView.setText(address);
      currentAddress = addressFeatures;
    }
  }
}
