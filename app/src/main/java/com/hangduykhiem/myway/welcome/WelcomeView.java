package com.hangduykhiem.myway.welcome;

import com.hangduykhiem.myway.core.BaseView;
import com.hangduykhiem.myway.data.model.AddressModel;
import com.hangduykhiem.myway.data.model.AddressModel.AddressFeatures;

/**
 * {@link BaseView} implementation of {@link WelcomeActivity}
 *
 * Created by dean on 16.4.2017.
 */

public interface WelcomeView extends BaseView {

  /**
   * Show autosuggestion
   *
   * @param response {@link AddressModel}
   */
  void showAutoSuggestion(AddressModel response);

  /**
   * Show the address on Google Map
   *
   * @param addressClicked {@link com.google.android.gms.maps.GoogleMap}
   */
  void showAddress(AddressFeatures addressClicked);

  /**
   * Location has been set, navigate to mainActivity
   */
  void finishSettingUp();

  /**
   * Location for home has been set, change UI element to display Work query
   */
  void finishSettingHome();

}
