package com.hangduykhiem.myway.welcome;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hangduykhiem.myway.R;
import com.hangduykhiem.myway.core.BaseActivity;
import com.hangduykhiem.myway.core.BasePresenter;
import com.hangduykhiem.myway.data.model.AddressModel;
import com.hangduykhiem.myway.data.model.AddressModel.AddressFeatures;
import com.hangduykhiem.myway.main.MainActivity;
import com.hangduykhiem.myway.utils.SimpleTextChangeListener;
import com.hangduykhiem.myway.welcome.adapter.AddressAutoSuggestionAdapter;

/**
 * Welcome activity, to input home and work address
 *
 * Created by dean on 16.4.2017.
 */
public class WelcomeActivity extends BaseActivity implements OnMapReadyCallback, WelcomeView {

  private WelcomePresenter welcomePresenter;
  private GoogleMap mapView;

  @BindView(R.id.welcome_address_list_view)
  RecyclerView recyclerView;
  @BindView(R.id.welcome_address_editText)
  EditText editText;
  @BindView(R.id.welcome_address_button)
  TextView buttonText;
  private AddressAutoSuggestionAdapter addressAutoSuggestionAdapter;
  private AddressFeatures currentlySelectedAddress;
  private CameraPosition originCameraPosition;

  public static void launch(Activity launchingActivity) {
    Intent intent = new Intent(launchingActivity, WelcomeActivity.class);
    launchingActivity.startActivity(intent);
  }

  @Override
  public void init(Bundle bundle) {

    // Initialize Map
    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);

    // Initialize original position
    LatLng helsinki = new LatLng(60.1699, 24.9384);
    originCameraPosition = new CameraPosition.Builder()
        .target(helsinki)      // Sets the center of the map to Helsinki
        .zoom(13)                   // Sets the zoom
        .build();                   // Creates a CameraPosition from the builder

    // Initialize recyclerView
    this.addressAutoSuggestionAdapter = new AddressAutoSuggestionAdapter(welcomePresenter);

    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(layoutManager);

    DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(
        recyclerView.getContext(),
        layoutManager.getOrientation());

    recyclerView.addItemDecoration(mDividerItemDecoration);
    recyclerView.setAdapter(addressAutoSuggestionAdapter);

    // Attach listener to detect editText event
    editText.addTextChangedListener(new SimpleTextChangeListener() {
      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        recyclerView.setVisibility(View.VISIBLE);

        if (s.length() > 0) {
          welcomePresenter.getAddressAutoSuggestion(s.toString());
        }
      }
    });
  }

  /**
   * Manipulates the map once available.
   * This callback is triggered when the map is ready to be used.
   * This is where we can add markers or lines, add listeners or move the camera. In this case,
   * we just add a marker near Sydney, Australia.
   * If Google Play services is not installed on the device, the user will be prompted to install
   * it inside the SupportMapFragment. This method will only be triggered once the user has
   * installed Google Play services and returned to the app.
   */
  @Override
  public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
    mapView = googleMap;
    mapView.getUiSettings().setCompassEnabled(true);
    mapView.getUiSettings().setZoomControlsEnabled(true);
    mapView.getUiSettings().setAllGesturesEnabled(true);

    mapView.animateCamera(CameraUpdateFactory.newCameraPosition(originCameraPosition), 1, null);
  }

  @Override
  public void showError(String s) {
    Toast.makeText(this, s, Toast.LENGTH_LONG).show();
  }

  @Override
  public void showLoading() {
  }

  @Override
  public void hideLoading() {
  }

  @Override
  protected int getDefaultView() {
    return R.layout.activity_welcome;
  }

  @NonNull
  @Override
  protected BasePresenter getPresenter() {
    if (welcomePresenter == null) {
      welcomePresenter = new WelcomePresenter(this);
    }
    return welcomePresenter;
  }

  @Override
  public void showAutoSuggestion(AddressModel response) {
    if (response != null) {
      addressAutoSuggestionAdapter.setNewSuggestion(response);
    }
  }

  @Override
  public void showAddress(AddressFeatures addressClicked) {
    currentlySelectedAddress = addressClicked;
    LatLng currentLatLng = new LatLng(addressClicked.geometry.coordinates.get(1),
        addressClicked.geometry.coordinates.get(0));
    addressAutoSuggestionAdapter.setNewSuggestion(null);
    if (mapView != null) {
      mapView.clear();
      mapView.addMarker(new MarkerOptions().position(currentLatLng));
      mapView.animateCamera(CameraUpdateFactory.newLatLng(currentLatLng));
    }
  }

  @Override
  public void finishSettingHome() {
    mapView.clear();
    mapView.animateCamera(CameraUpdateFactory.newCameraPosition(originCameraPosition));
    editText.setHint(R.string.welcome_home_work_hint);
    editText.setText("");
    buttonText.setText(R.string.welcome_add_work_address);
  }

  @Override
  public void finishSettingUp() {
    MainActivity.launch(this);
    finish();
  }

  @OnClick(R.id.welcome_address_button)
  public void addAddress() {
    if (currentlySelectedAddress != null) {
      welcomePresenter.setAddress(currentlySelectedAddress);
      Toast.makeText(this, getString(R.string.a_welcome_address_accept),
          Toast.LENGTH_SHORT).show();
      currentlySelectedAddress = null;
    } else {
      String errorString = getString(R.string.e_welcome_no_address_selcted);
      showError(errorString);
    }
  }
}
