package com.hangduykhiem.myway.welcome;

import android.content.Context;
import android.os.Bundle;
import com.hangduykhiem.myway.SettingsHelper;
import com.hangduykhiem.myway.core.BasePresenter;
import com.hangduykhiem.myway.data.HslRepository;
import com.hangduykhiem.myway.data.model.AddressModel;
import com.hangduykhiem.myway.data.model.AddressModel.AddressFeatures;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Presenter for WelcomeActivity
 *
 * Created by dean on 16.4.2017.
 */
public class WelcomePresenter extends BasePresenter<WelcomeView> {

  private HslRepository hslRepository;
  private SettingsHelper settingsHelper;
  private Context context;
  private boolean isHomeSet;

  WelcomePresenter(Context context) {
    this.context = context;
  }

  @Override
  public void initialise(Bundle state) {
    super.initialise(state);
    hslRepository = HslRepository.getInstance();
    settingsHelper = SettingsHelper.getInstance(context);
    isHomeSet = false;
  }

  public void getAddressAutoSuggestion(final String s) {
    hslRepository.getAddressSuggestion(s, new Callback<AddressModel>() {
      @Override
      public void onResponse(Call<AddressModel> call, Response<AddressModel> response) {
        view.showAutoSuggestion(response.body());
      }

      @Override
      public void onFailure(Call<AddressModel> call, Throwable t) {
        view.showError(t.getMessage());
        t.printStackTrace();
      }
    });
  }

  public void onAddressSuggestionClick(AddressFeatures addressClicked){
    view.showAddress(addressClicked);
  }

  public void setAddress(AddressFeatures currentAddress) {
    if (!isHomeSet) {
      settingsHelper.setHomeLocation(currentAddress.geometry.coordinates.get(1),
          currentAddress.geometry.coordinates.get(0));
      isHomeSet = true;
      view.finishSettingHome();
    } else {
      settingsHelper.setWorkLocation(currentAddress.geometry.coordinates.get(1),
          currentAddress.geometry.coordinates.get(0));
      settingsHelper.setFirstTime(false);
      view.finishSettingUp();
    }
  }
}
