package com.hangduykhiem.myway;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * SettingsHelper to get and set current setting of the application
 *
 * Created by dean on 22.4.2017.
 */

public class SettingsHelper {

  private static final String PARAM_FIRST_TIME = "SettingsHelper:firstTime";
  private static final String PARAM_HOME = "SettingsHelper:home";
  private static final String PARAM_WORK = "SettingsHelper:work";

  private SharedPreferences sharedPreferences;
  private static SettingsHelper settingsHelper;
  private static Gson gson;

  private SettingsHelper(Context context) {
    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

  }

  public static SettingsHelper getInstance(Context context) {
    if (settingsHelper == null) {
      settingsHelper = new SettingsHelper(context);
    }
    if (gson == null) {
      gson = new GsonBuilder().create();
    }
    return new SettingsHelper(context);
  }

  public boolean getFirstTime() {
    return sharedPreferences.getBoolean(PARAM_FIRST_TIME, true);
  }

  public void setFirstTime(boolean firstTime) {
    sharedPreferences.edit().putBoolean(PARAM_FIRST_TIME, firstTime).apply();
  }

  public void setHomeLocation(double lat, double lon) {
    LatLng home = new LatLng(lat, lon);
    String json = gson.toJson(home);
    sharedPreferences.edit().putString(PARAM_HOME, json).apply();
  }

  public LatLng getHomeLocattion() {
    String json = sharedPreferences.getString(PARAM_HOME, "");
    return gson.fromJson(json, LatLng.class);
  }

  public void setWorkLocation(double lat, double lon) {
    LatLng home = new LatLng(lat, lon);
    String json = gson.toJson(home);
    sharedPreferences.edit().putString(PARAM_WORK, json).apply();
  }

  public LatLng getWorkLocattion() {
    String json = sharedPreferences.getString(PARAM_WORK, "");
    return gson.fromJson(json, LatLng.class);
  }
}
