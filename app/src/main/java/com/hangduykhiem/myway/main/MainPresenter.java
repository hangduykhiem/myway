package com.hangduykhiem.myway.main;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.google.android.gms.maps.model.LatLng;
import com.hangduykhiem.myway.SettingsHelper;
import com.hangduykhiem.myway.core.BasePresenter;
import com.hangduykhiem.myway.data.HslRepository;
import com.hangduykhiem.myway.data.LocationRepository;
import com.hangduykhiem.myway.data.LocationRepository.LocationCallback;
import com.hangduykhiem.myway.data.WeatherRepository;
import com.hangduykhiem.myway.data.model.AddressModel;
import com.hangduykhiem.myway.data.model.RouteModel;
import com.hangduykhiem.myway.data.model.WeatherForecastModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Presenter implementation for MainView
 *
 * Created by dean on 28.2.2017.
 */
public class MainPresenter extends BasePresenter<MainView> {

  private LocationRepository locationRepository;
  private WeatherRepository weatherRepository;
  private HslRepository hslRepository;
  private final Context context;
  private Location location;
  private SettingsHelper settingsHelper;
  private LatLng homeLocation;
  private LatLng workLocation;

  public MainPresenter(Context context) {
    this.context = context;
  }

  @Override
  public void initialise(Bundle state) {
    super.initialise(state);

    settingsHelper = SettingsHelper.getInstance(context);

    if (settingsHelper.getFirstTime()) {
      view.navigateToWelcome();
    } else {
      homeLocation = settingsHelper.getHomeLocattion();
      workLocation = settingsHelper.getWorkLocattion();
      weatherRepository = new WeatherRepository();
      hslRepository = HslRepository.getInstance();
      locationRepository = new LocationRepository(context, new LocationCallback() {
        @Override
        public void onError(Exception e) {
          view.showError("Can't connect to LocationRepo");
        }

        @Override
        public void onReceiveNewLocation(Location location) {
          MainPresenter.this.location = location;
          view.hideLoading();
          // Save battery yey.
          stopLocationRequest();
          getWeather();
          getAddress();
          loadRoadHome();
          loadRoadWork();
        }

        @Override
        public void onConnectionSuspended() {
        }

        @Override
        public void onNoPermission() {
          view.requestPermission();
          view.hideLoading();
        }
      });

      view.showLoading();
      locationRepository.connectGoogleApi();
    }
  }

  private void getWeather() {
    weatherRepository.getWeather(location, new Callback<WeatherForecastModel>() {
      @Override
      public void onResponse(Call<WeatherForecastModel> call,
          Response<WeatherForecastModel> response) {
        view.showWeather(response.body());
      }

      @Override
      public void onFailure(Call<WeatherForecastModel> call, Throwable t) {
        view.showError(t.getMessage());
      }
    });
  }

  private void getAddress() {
    hslRepository.getLocationAddress(location, new Callback<AddressModel>() {
      @Override
      public void onResponse(Call<AddressModel> call, Response<AddressModel> response) {
        view.showLocation(response.body());
      }

      @Override
      public void onFailure(Call<AddressModel> call, Throwable t) {
        view.showError(t.getMessage());
      }
    });
  }

  public void suspendAPIconnect() {
    if (locationRepository != null) {
      locationRepository.disconnectGoogleApi();
    }
  }

  public void loadRoadHome() {
    hslRepository
        .getRoute(new LatLng(location.getLatitude(), location.getLongitude()), homeLocation,
            new Callback<RouteModel>() {
              @Override
              public void onResponse(Call<RouteModel> call, Response<RouteModel> response) {
                view.showRouteHome(response.body());
              }

              @Override
              public void onFailure(Call<RouteModel> call, Throwable t) {
                view.showError(t.getMessage());
              }
            });
  }

  public void loadRoadWork() {
    hslRepository
        .getRoute(new LatLng(location.getLatitude(), location.getLongitude()), workLocation,
            new Callback<RouteModel>() {
              @Override
              public void onResponse(Call<RouteModel> call, Response<RouteModel> response) {
                view.showRouteToWork(response.body());
              }

              @Override
              public void onFailure(Call<RouteModel> call, Throwable t) {
                view.showError(t.getMessage());
              }
            });
  }

  public void stopLocationRequest() {
    locationRepository.stopLocationRequest();
  }

  public void reDoLocation() {

    view.showLoading();
    locationRepository.doLocationRequest();
  }
}
