package com.hangduykhiem.myway.main;

import com.hangduykhiem.myway.core.BaseView;
import com.hangduykhiem.myway.data.model.AddressModel;
import com.hangduykhiem.myway.data.model.RouteModel;
import com.hangduykhiem.myway.data.model.WeatherForecastModel;

/**
 * {@link BaseView} implementation for MainActivity
 *
 * Created by dean on 28.2.2017.
 */

public interface MainView extends BaseView {

  void showLocation(AddressModel addressModel);

  void requestPermission();

  void showWeather(WeatherForecastModel weatherForecastModel);

  void showRouteHome(RouteModel body);

  void showRouteToWork(RouteModel body);

  void navigateToWelcome();
}
