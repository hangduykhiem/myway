package com.hangduykhiem.myway.main;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import com.hangduykhiem.myway.R;
import com.hangduykhiem.myway.core.BaseActivity;
import com.hangduykhiem.myway.core.BasePresenter;
import com.hangduykhiem.myway.core.DateUtils;
import com.hangduykhiem.myway.data.model.AddressModel;
import com.hangduykhiem.myway.data.model.RouteModel;
import com.hangduykhiem.myway.data.model.RouteModel.RouteItineraryLegModel;
import com.hangduykhiem.myway.data.model.RouteModel.RouteItineraryModel;
import com.hangduykhiem.myway.data.model.WeatherForecastModel;
import com.hangduykhiem.myway.welcome.WelcomeActivity;
import java.util.List;

/**
 * First activity where user get to.
 */
public class MainActivity extends BaseActivity implements MainView {

  private static final int PERMISSION_LOCATION = 1;
  private MainPresenter mainPresenter;
  private boolean isResetting;
  private RouteModel routeHome;
  private RouteModel routeWork;

  @BindView(R.id.location_text)
  TextView locationText;
  @BindView(R.id.home_text)
  TextView homeText;
  @BindView(R.id.work_text)
  TextView workText;
  @BindView(R.id.weather_text)
  TextView weatherText;
  @BindView(R.id.main_progress_bar)
  ProgressBar progressBar;
  @BindView(R.id.work_container)
  CardView workContainer;
  @BindView(R.id.home_container)
  CardView homeContainer;


  public static void launch(Activity launchingActivity) {
    Intent intent = new Intent(launchingActivity, MainActivity.class);
    launchingActivity.startActivity(intent);
  }

  @Override
  public void showLocation(AddressModel location) {
    if (isResetting) {
      Toast.makeText(this, "New Location received", Toast.LENGTH_SHORT).show();
      isResetting = false;
    }
    locationText.setText(location.toString());
  }

  @Override
  public void requestPermission() {
    String[] permission = new String[1];
    permission[0] = Manifest.permission.ACCESS_FINE_LOCATION;
    ActivityCompat.requestPermissions(this,
        permission, PERMISSION_LOCATION);
  }

  @Override
  public void showWeather(WeatherForecastModel weatherForecastModel) {
    String weatherString =
        weatherForecastModel.currently.temperature + " C, "
            + weatherForecastModel.currently.summary;
    weatherText.setText(weatherString);
  }

  @Override
  public void showRouteHome(RouteModel body) {

    try {
      StringBuilder routeTextBuilder = new StringBuilder();
      routeTextBuilder.append("To home: ");
      RouteItineraryModel itineraryModel = body.dataModel.routePlanModel.routeItineraryModelList
          .get(0);
      List<RouteItineraryLegModel> legs = itineraryModel.routeItineraryLegsList;
      // Get first transport
      for (RouteItineraryLegModel leg : legs) {
        if (leg.legRouteModel != null) {
          routeTextBuilder.append(leg.legRouteModel.shortName);
        }
      }

      if (itineraryModel.ininerariesStartTime != 0) {
        routeTextBuilder.append(", starting at ")
            .append(DateUtils.formatUnixTime(itineraryModel.ininerariesStartTime, "HH:mm"));
      }

      homeText.setText(routeTextBuilder.toString());
    } catch (Exception e) {
      Log.e("MainActivity", "Error parsing route", e);
      homeText.setText("No Route");
    }
  }

  @Override
  public void showRouteToWork(RouteModel body) {
    try {
      StringBuilder routeTextBuilder = new StringBuilder();

      routeTextBuilder.append("To work: ");

      RouteItineraryModel itineraryModel = body.dataModel.routePlanModel.routeItineraryModelList
          .get(0);
      List<RouteItineraryLegModel> legs = itineraryModel.routeItineraryLegsList;

      // Get first transport
      String spacing = "";
      for (RouteItineraryLegModel leg : legs) {
        if (leg.legRouteModel != null) {
          routeTextBuilder.append(leg.legRouteModel.shortName);
          routeTextBuilder.append(spacing);
          spacing = " + ";
        }
      }

      if (itineraryModel.ininerariesStartTime != 0) {
        routeTextBuilder.append(", starting at ")
            .append(DateUtils.formatUnixTime(itineraryModel.ininerariesStartTime, "HH:mm"));
      }

      workText.setText(routeTextBuilder.toString());
    } catch (Exception e) {
      Log.e("MainActivity", "Error parsing route", e);
      workText.setText("No Route");
    }
  }

  @Override
  public void navigateToWelcome() {
    WelcomeActivity.launch(this);
    finish();
  }

  @Override
  public void onRequestPermissionsResult(int requestCode,
      @NonNull String permissions[], @NonNull int[] grantResults) {
    switch (requestCode) {
      case PERMISSION_LOCATION: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0
            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

          mainPresenter.reDoLocation();
        } else {
          Toast.makeText(this, "Can't do anything without location", Toast.LENGTH_SHORT).show();
        }
        break;
      }
      default: {
        Toast.makeText(this, "WTF we didn't even request this permission", Toast.LENGTH_SHORT)
            .show();
      }
    }
  }

  @Override
  public void init(Bundle bundle) {
  }

  @Override
  public void showError(String s) {
    Toast.makeText(this, s, Toast.LENGTH_LONG).show();
  }

  @Override
  public void showLoading() {
    progressBar.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideLoading() {
    progressBar.setVisibility(View.INVISIBLE);
  }

  @Override
  protected int getDefaultView() {
    return R.layout.activity_main;
  }

  @NonNull
  @Override
  protected BasePresenter getPresenter() {
    if (mainPresenter == null) {
      mainPresenter = new MainPresenter(this);
    }

    return mainPresenter;
  }

  @OnClick(R.id.refreshButton)
  public void reset() {
    mainPresenter.reDoLocation();
    isResetting = true;
  }

  @Override
  protected void onStop() {
    mainPresenter.suspendAPIconnect();
    super.onStop();
  }
}
