package com.hangduykhiem.myway.data.model;

import java.util.List;

/**
 * Created by dean on 2.3.2017.
 */
public class AddressModel {

  public List<AddressFeatures> features;

  public class AddressFeatures {

    public AddressProperties properties;
    public AddressGeometry geometry;
  }

  public class AddressGeometry {

    public List<Double> coordinates;
  }

  public class AddressProperties {

    public String name;
    public String postalcode;
    public String locality;
  }

  @Override
  public String toString() {

    AddressFeatures bestAddress = features.get(0);

    return bestAddress.properties.name + ", " + bestAddress.properties.postalcode + ", "
        + bestAddress.properties.locality;
  }
}
