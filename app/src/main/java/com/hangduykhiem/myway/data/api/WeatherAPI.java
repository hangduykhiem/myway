package com.hangduykhiem.myway.data.api;

import com.hangduykhiem.myway.data.model.WeatherForecastModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Weather API interface
 *
 * Created by dean on 1.3.2017.
 */
public interface WeatherAPI {

  @GET("forecast/{key}/{location}?units=si")
  Call<WeatherForecastModel> getWeatherModel(@Path("key") String apiKey,
      @Path("location") String locationString);

}
