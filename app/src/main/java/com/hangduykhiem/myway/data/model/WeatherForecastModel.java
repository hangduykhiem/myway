package com.hangduykhiem.myway.data.model;

/**
 * Created by dean on 1.3.2017.
 */

public class WeatherForecastModel {
  public WeatherTimeModel currently;

  public class WeatherTimeModel {
    public String summary;
    public String temperature;
  }
}
