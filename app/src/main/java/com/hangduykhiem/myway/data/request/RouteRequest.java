package com.hangduykhiem.myway.data.request;

/**
 * Model to request the route from the HSL service
 *
 * Created by dean on 2.3.2017.
 */
public class RouteRequest {

  public String query;

  public RouteRequest(String query) {
    this.query = query;
  }

  public static class QueryBuilder {

    private String startLocation;
    private String goalLocation;

    public QueryBuilder setStartLocation(Double lat, Double lon) {
      this.startLocation = "from: {lat:" + lat +
          ", lon:" + lon + "} ";
      return this;
    }

    public QueryBuilder setGoalLocation(Double lat, Double lon) {
      this.goalLocation = "to: {lat:" + lat +
          ", lon:" + lon + "} ";
      return this;
    }

    public RouteRequest build() {
      return new RouteRequest("{ "
          + "plan( "
          + startLocation
          + goalLocation
          + "numItineraries: 3"
          + ") { "
          + "itineraries { "
          + "startTime "
          + "legs { "
          + "route{ "
          + "shortName "
          + "longName "
          + "} "
          + "from { "
          + "name "
          + "}"
          + "startTime "
          + "endTime "
          + "} "
          + "} "
          + "} "
          + "}");
    }
  }

}
