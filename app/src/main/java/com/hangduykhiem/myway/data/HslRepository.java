package com.hangduykhiem.myway.data;

import android.location.Location;
import android.util.Log;
import com.google.android.gms.maps.model.LatLng;
import com.hangduykhiem.myway.RetrofitHelper;
import com.hangduykhiem.myway.data.api.HslAPI;
import com.hangduykhiem.myway.data.model.AddressModel;
import com.hangduykhiem.myway.data.model.RouteModel;
import com.hangduykhiem.myway.data.request.RouteRequest;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Repository to request HSL data
 *
 * Created by dean on 2.3.2017.
 */

public class HslRepository {

  private static HslRepository instance;
  private final HslAPI hslAPI;

  private HslRepository() {
    hslAPI = RetrofitHelper.getHslRetrofit().create(HslAPI.class);
  }

  public static HslRepository getInstance() {
    if (instance == null) {
      instance = new HslRepository();
    }
    return instance;
  }

  public void getLocationAddress(Location location,
      Callback<AddressModel> weatherForecastModelCallback) {

    Call<AddressModel> call = hslAPI
        .getGeoCodingAddress(location.getLatitude(), location.getLongitude());
    call.enqueue(weatherForecastModelCallback);
  }

  public void getAddressSuggestion(String query, Callback<AddressModel> addressModelCallback) {

    Call<AddressModel> call = hslAPI.getAddressSuggestion(query);
    call.enqueue(addressModelCallback);

  }

  public void getRoute(LatLng from, LatLng to, Callback<RouteModel> routeModelCallback) {
    RouteRequest query = new RouteRequest.QueryBuilder()
        .setStartLocation(from.latitude, from.longitude)
        .setGoalLocation(to.latitude, to.longitude).build();
    Log.d(this.getClass().toString(), "Request Kamppi: " + query.query);
    Call<RouteModel> call = hslAPI.getRouteModel(query);
    call.enqueue(routeModelCallback);
  }
}
