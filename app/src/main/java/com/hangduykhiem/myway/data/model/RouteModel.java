package com.hangduykhiem.myway.data.model;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Model to display route from one point to another
 *
 * Created by dean on 6.3.2017.
 */

public class RouteModel {

  @SerializedName("data")
  public RouteDataModel dataModel;

  public class RouteDataModel{
    @SerializedName("plan")
    public RoutePlanModel routePlanModel;
  }

  public class RoutePlanModel{
    @SerializedName("itineraries")
    public List<RouteItineraryModel> routeItineraryModelList;
  }

  public class RouteItineraryModel {
    @SerializedName("legs")
    public List<RouteItineraryLegModel> routeItineraryLegsList;
    @SerializedName("startTime")
    public long ininerariesStartTime;
  }

  public class RouteItineraryLegModel {
    @SerializedName("route")
    public LegRouteModel legRouteModel;
    public long startTime;
    public long endTime;
  }

  public class LegRouteModel {
    public String longName;
    public String shortName;
  }
}
