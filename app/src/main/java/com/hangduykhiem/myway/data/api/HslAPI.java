package com.hangduykhiem.myway.data.api;

import com.hangduykhiem.myway.data.model.AddressModel;
import com.hangduykhiem.myway.data.model.RouteModel;
import com.hangduykhiem.myway.data.request.RouteRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Retrofit Interface for HSL
 *
 * Created by dean on 1.3.2017.
 */

public interface HslAPI {

  @GET("geocoding/v1/reverse/?size=1")
  Call<AddressModel> getGeoCodingAddress(@Query("point.lat") Double lat,
      @Query("point.lon") Double lon);

  @GET("geocoding/v1/search?"
      + "boundary.rect.min_lat=59.93&"
      + "boundary.rect.max_lat=60.42&"
      + "boundary.rect.min_lon=24.23&"
      + "boundary.rect.max_lon=25.69&"
      + "size=3")
  Call<AddressModel> getAddressSuggestion(@Query("text") String query);

  @POST("routing/v1/routers/hsl/index/graphql")
  Call<RouteModel> getRouteModel(@Body RouteRequest routeRequest);
}
