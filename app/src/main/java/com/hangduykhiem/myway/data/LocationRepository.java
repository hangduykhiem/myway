package com.hangduykhiem.myway.data;

import android.Manifest.permission;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import java.net.ConnectException;

/**
 * Implementation of locationRepository
 *
 * Created by dean on 28.2.2017.
 */

public class LocationRepository implements ConnectionCallbacks,
    OnConnectionFailedListener,
    LocationListener {

  private static final long UPDATE_INTERVAL = 5;
  private static final long FASTEST_INTERVAL = 1;
  private GoogleApiClient googleApiClient;
  private Context context;
  private LocationCallback locationCallback;

  public interface LocationCallback {

    void onError(Exception e);

    void onReceiveNewLocation(Location location);

    void onConnectionSuspended();

    void onNoPermission();
  }

  public LocationRepository(Context context, LocationCallback locationCallback) {

    this.context = context;
    this.locationCallback = locationCallback;
  }

  // Connect to google Api
  public void connectGoogleApi() {

    // Build a new google api client if it's not build already.
    if (googleApiClient == null) {
      googleApiClient = new GoogleApiClient.Builder(context)
          .addConnectionCallbacks(this)
          .addOnConnectionFailedListener(this)
          .addApi(LocationServices.API)
          .build();
    }

    // connect it
    googleApiClient.connect();
  }

  /**
   * Disconnect the Google API, then clear the reference, so that it won't leak
   */
  public void disconnectGoogleApi() {
    if (googleApiClient != null) {
      googleApiClient.disconnect();
      googleApiClient = null;
    }
  }

  @Override
  public void onConnected(@Nullable Bundle bundle) {

    doLocationRequest();
  }

  public void doLocationRequest() {

    if (googleApiClient == null || !googleApiClient.isConnected()) {
      connectGoogleApi();
    } else {

      final LocationRequest locationRequest = new LocationRequest();
      // Use high accuracy
      locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
      // Set the update interval to 5 seconds
      locationRequest.setInterval(UPDATE_INTERVAL);
      // Set the fastest update interval to 1 second
      locationRequest.setFastestInterval(FASTEST_INTERVAL);

      if (ActivityCompat.checkSelfPermission(context, permission.ACCESS_FINE_LOCATION)
          != PackageManager.PERMISSION_GRANTED
          && ActivityCompat.checkSelfPermission(context, permission.ACCESS_COARSE_LOCATION)
          != PackageManager.PERMISSION_GRANTED) {

        locationCallback.onNoPermission();
        return;
      }
      LocationServices.FusedLocationApi.requestLocationUpdates(
          googleApiClient, locationRequest, this);
    }
  }

  /**
   * Stop the update
   */
  public void stopLocationRequest() {
    LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
  }

  @Override
  public void onConnectionSuspended(int i) {
    locationCallback.onConnectionSuspended();
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    locationCallback.onError(new ConnectException("Cannot connect to GooglePlay service"));
  }

  @Override
  public void onLocationChanged(Location location) {
    locationCallback.onReceiveNewLocation(location);
  }
}
