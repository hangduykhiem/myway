package com.hangduykhiem.myway.data;

import android.location.Location;
import com.hangduykhiem.myway.BuildConfig;
import com.hangduykhiem.myway.RetrofitHelper;
import com.hangduykhiem.myway.data.api.WeatherAPI;
import com.hangduykhiem.myway.data.model.WeatherForecastModel;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Repository to get weather data
 *
 * Created by dean on 1.3.2017.
 */

public class WeatherRepository {

  private final WeatherAPI weatherAPI;

  public WeatherRepository() {
    weatherAPI = RetrofitHelper.getWeatherRetrofit().create(WeatherAPI.class);
  }

  public void getWeather(Location location,
      Callback<WeatherForecastModel> weatherForecastModelCallable) {

    Call<WeatherForecastModel> call = weatherAPI.getWeatherModel(BuildConfig.WEATHER_KEY,
        location.getLatitude() + "," + location.getLongitude());
    call.enqueue(weatherForecastModelCallable);
  }
}
