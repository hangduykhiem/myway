package com.hangduykhiem.myway;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dean on 1.3.2017.
 */

public class RetrofitHelper {

  private static Retrofit weatherRetrofit;
  private static Retrofit hslRetrofit;

  private RetrofitHelper() {
  }

  public static Retrofit getWeatherRetrofit() {
    if (weatherRetrofit == null) {
      weatherRetrofit = new Retrofit.Builder()
          .baseUrl("https://api.darksky.net/")
          .addConverterFactory(GsonConverterFactory.create())
          .build();
    }
    return weatherRetrofit;
  }

  public static Retrofit getHslRetrofit() {
    if (hslRetrofit == null) {
      hslRetrofit = new Retrofit.Builder()
          .baseUrl("https://api.digitransit.fi/")
          .addConverterFactory(GsonConverterFactory.create())
          .build();
    }
    return hslRetrofit;
  }
}
