package com.hangduykhiem.myway.utils;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * A simple class that has all method in TextWatcher implemented, so that the caller only need to
 * override the method needed
 *
 * Created by dean on 21.4.2017.
 */

public class SimpleTextChangeListener implements TextWatcher {

  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after) {

  }

  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count) {

  }

  @Override
  public void afterTextChanged(Editable s) {

  }
}
