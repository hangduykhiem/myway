package com.hangduykhiem.myway.core;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import butterknife.ButterKnife;

/**
 * BaseActivity for this whole project
 *
 * Created by dean on 28.2.2017.
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseView {

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(getDefaultView());
    ButterKnife.bind(this);
    //noinspection unchecked
    getPresenter().setView(this);
    getPresenter().initialise(savedInstanceState);
  }

  protected abstract int getDefaultView();

  @NonNull
  protected abstract BasePresenter getPresenter();
}
