package com.hangduykhiem.myway.core;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by dean on 15.4.2017.
 */

public class DateUtils {

  /**
   * Private constructor to prevent initialization of a utils class
   */
  private DateUtils() {
  }

  /**
   * Get the unixTime input formatted according to the pattern input
   *
   * @param input a long as UnixTime
   * @param pattern {@link String} as a pattern to format
   * @return {@link String} as formatted according to pattern
   */
  public static String formatUnixTime(long input, String pattern) {

    SimpleDateFormat format = new SimpleDateFormat(pattern, Locale.getDefault());
    Date date = new Date(input); // *1000 is to convert seconds to milliseconds
    return format.format(date);
  }
}
