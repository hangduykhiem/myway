package com.hangduykhiem.myway.core;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

/**
 * BasePresenter as P in MVP
 *
 * Created by dean on 28.2.2017.
 */
public abstract class BasePresenter<T extends BaseView> {

  protected T view;

  /**
   * Constructor
   */
  protected BasePresenter() {
  }

  @CallSuper
  public void initialise(Bundle state) {

    // Init View
    view.init(state);
  }

  /**
   * Saves the instance of the {@link BasePresenter}
   *
   * @param outState {@link Bundle} to save the state
   */
  @SuppressWarnings("UnusedParameters")
  @CallSuper
  public void onSaveInstanceState(Bundle outState) {

  }

  /**
   * Sets the {@link T} instance to link the {@link BasePresenter} with the UI so that the presenter
   * can hold a reference to the view it will be interacting with.
   *
   * @param view {@link T} to link the {@link BasePresenter} with the UI
   */
  public final void setView(@NonNull T view) {

    this.view = view;
  }

}