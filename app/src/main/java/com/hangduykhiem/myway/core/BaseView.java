package com.hangduykhiem.myway.core;

import android.os.Bundle;
import com.hangduykhiem.myway.data.model.AddressModel;
import retrofit2.Response;

/**
 * BaseView in the MVP
 *
 * Created by dean on 28.2.2017.
 */

public interface BaseView {

  /**
   * Init view. This is call after BasePresenter is set.
   */
  void init(Bundle bundle);

  /**
   * Show error
   * @param s error that should shows to users
   */
  void showError(String s);

  /**
   * Show a loading progressbar;
   */
  void showLoading();

  /**
   * Hide loading;
   */
  void hideLoading();
}
