#**MYWAY**#
The one app for your morning

### #WhatIsMyWay? 
We understand that everybody have those hectic morning. From crying children to late night gaming, there are a lot of factors that keep you late to work. One of those factor that we want to solve, is in fact the way you navigate to work. 

MyWay, unlike every other navigating application, save your home address and work (school) address in its memory. All you need to do is click the button, and the way to work will appears, along with the weather outside (just so that you won't be too cold). 

### How do I get set up?

* Fork this repo
* Clone it to your local machine
* Edit it
* Make a PR

### Contribution guidelines ###

* Install [Intellij Google Java Style(https://github.com/google/styleguide)
* Install Intellij CheckStyle Plugin and add [Google CheckStyle](http://checkstyle.sourceforge.net/google_style.html)
* Run Checkstyle and format, as well as import optimization before send PR

### 3rd party licenses

Retrofit - Apache 2.0

ButterKnife - Apache 2.0

Android support library - Apache 2.0

### Contact

Email: HangDuyKhiem@Gmail.com